>Gulp版本4.02

- 下载gulp

```
npm i gulp
```

- 新建 gulpfile.js 文件

```js
const gulp = require('gulp')

gulp.task('first', async () => {
 await console.log('第一个gulp任务');
  gulp.src('./src/index.css').pipe(gulp.dest('dist/css'))
})
```

gulp中提供的方法

- gulp.src() 获取任务要处理的文件
- gulp.dest() 输出文件
- gulp.task() 建立gulp任务
- gulp.watch() 监控文件的变化

### gulp 插件

- gulp-htmlin:html文件压缩
- gulp-csso:压缩css
- gulp-babel:js语法转化
- gulp-less:less语法转化
- gulp-uglify:压缩混淆js
- gulp-file-include:构建公用头部尾部
- browsersync:浏览器实时同步

我们将公共结构删除掉。新建一个文件，写入内容。
引入的时候`@@include('./common/header.html')`
然后执行任务就可以了。

### 注意
gulp4最大的变化是不能像以前那样传递一个依赖的任务列表,你需要使用`gulp.series`和`gulp.parallel`，因为gulp任务现在只有两个参数。

- gulp.series：按照先后顺序执行

- gulp.parallel：可以并行执行

gulp4执行代码如下
```js
gulp.task('default', gulp.series('htmlmin', 'cssmin', 'jsmin', 'copy'), async () => {
})

gulp.task('build',gulp.parallel('style','script','images',() => {
    
}));
```
或者
```js
gulp.task('default',gulp.series('a',gulp.parallel('style','script','image'),'b','c',() => {
    
}));
```

其他详情请见：`gulpfile.js`

