/*
 * @Descripttion: 
 * @version: 
 * @Author: yang_ft
 * @Date: 2020-10-24 12:19:39
 * @github: famensaodiseng
 * @LastEditTime: 2020-10-24 15:29:54
 */
const gulp = require('gulp')
const htmlmin = require('gulp-htmlmin')
const fileinclude = require('gulp-file-include')
const less = require('gulp-less')
const csso = require('gulp-csso')
const babel = require('gulp-babel')
const uglify = require('gulp-uglify')

gulp.task('first', async () => {
  await console.log('第一个gulp任务');
  gulp.src('./src/index.css')
    .pipe(gulp.dest('dist/css'));
})

//html 任务
// 1.抽取公共代码
// 2.html代码压缩
gulp.task('htmlmin', async () => {
  await gulp.src('./src/*.html')
    .pipe(fileinclude())
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('dist'));
  //压缩代码
})


//css 任务
// 1.less语法转化  
// 2. css代码压缩
gulp.task('cssmin', async () => {
  //选择css目录下面所有less 文件以及css文件
  await gulp.src(['./src/css/*.less', './src/css/*.css'])
    //将less 转化为css语法
    .pipe(less())
    //将css代码压缩
    .pipe(csso())
    //输出处理结果
    .pipe(gulp.dest('dist/css'));
  //压缩代码
})


// js 任务
// 1.es6代码转化  
// 2. js代码压缩
gulp.task('jsmin', async () => {
  //选择js目录下面所有js 
  await gulp.src('./src/js/*.js')
    //将es6 转化为es5语法
    .pipe(babel({
      presets: ['@babel/env']
    }))
    //将js代码压缩
    //输出处理结果
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'));
  //压缩代码
})

// 复制文件夹
gulp.task('copy', async () => {
  await gulp.src('./src/images/*')
    .pipe(gulp.dest('dist/images'));
  gulp.src('./src/lib/*')
    .pipe(gulp.dest('dist/lib'));
});


// 构建任务
gulp.task('default', gulp.series('htmlmin', 'cssmin', 'jsmin', 'copy'), async () => {
})
